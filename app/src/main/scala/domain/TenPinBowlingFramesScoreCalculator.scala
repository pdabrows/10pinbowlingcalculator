
package domain

import domain.FrameState.{IN_PROGRESS, OPEN, SCORE_CALCULATED, SPARE, STRIKE}

class TenPinBowlingFramesScoreCalculator() {

  def recalculateFramesScore(resultHolder: TenPinBowlingResultHolder): TenPinBowlingResultHolder = {
    resultHolder.copy(
      frameList = calculateFirst8Frames(resultHolder)
        .concat(calculateLastTwoFrames(resultHolder)))
  }

  private def calculateLastTwoFrames(resultHolder: TenPinBowlingResultHolder) = {
    val twoLastFrames = resultHolder.getFrameList.takeRight(2)
    val ninthFrame = twoLastFrames.head
    val tenthFrame = twoLastFrames.last

    val new9Frame: Frame = (ninthFrame.frameState, tenthFrame.frameState, resultHolder.getFirstExtraThrow) match {
      case (STRIKE, STRIKE, Some(_)) => Frame(ninthFrame.number, ninthFrame.firstThrow, ninthFrame.secondThrow, Option(ninthFrame.firstThrow.get + tenthFrame.firstThrow.get + resultHolder.getFirstExtraThrow.get), SCORE_CALCULATED)
      case (STRIKE, OPEN | SPARE, _) => Frame(ninthFrame.number, ninthFrame.firstThrow, ninthFrame.secondThrow, Option(ninthFrame.firstThrow.get + tenthFrame.firstThrow.get + tenthFrame.secondThrow.get), SCORE_CALCULATED)
      case (SPARE, OPEN | SPARE | STRIKE, _) => Frame(ninthFrame.number, ninthFrame.firstThrow, ninthFrame.secondThrow, Option(ninthFrame.firstThrow.get + ninthFrame.secondThrow.get + tenthFrame.firstThrow.get), SCORE_CALCULATED)
      case (OPEN, _, _) => Frame(ninthFrame.number, ninthFrame.firstThrow, ninthFrame.secondThrow, Option(ninthFrame.firstThrow.get + ninthFrame.secondThrow.get), SCORE_CALCULATED)
      case _ => ninthFrame
    }
    val new10Frame: Frame = (tenthFrame.frameState, resultHolder.getFirstExtraThrow, resultHolder.getSecondExtraThrow) match {
      case (STRIKE, Some(_), Some(_)) => Frame(tenthFrame.number, tenthFrame.firstThrow, tenthFrame.secondThrow, Option(tenthFrame.firstThrow.get + resultHolder.getFirstExtraThrow.get + resultHolder.getSecondExtraThrow.get), SCORE_CALCULATED)
      case (SPARE, Some(_), _) => Frame(tenthFrame.number, tenthFrame.firstThrow, tenthFrame.secondThrow, Option(tenthFrame.firstThrow.get + tenthFrame.secondThrow.get + resultHolder.getFirstExtraThrow.get), SCORE_CALCULATED)
      case (OPEN, _, _) => Frame(tenthFrame.number, tenthFrame.firstThrow, tenthFrame.secondThrow, Option(tenthFrame.firstThrow.get + tenthFrame.secondThrow.get), SCORE_CALCULATED)
      case _ => tenthFrame
    }

    List[Frame](new9Frame, new10Frame)

  }

  private def calculateFirst8Frames(holder: TenPinBowlingResultHolder) = {
    holder.getFrameList.sliding(3)
      .map(sublist => {
        println(sublist)
        val current = sublist.head
        val next = sublist.tail.head
        val last = sublist.last
        (current.frameState, next.frameState, last.frameState) match {
          case (STRIKE, STRIKE, STRIKE | SPARE | OPEN | IN_PROGRESS) => Frame(current.number, current.firstThrow, Option.empty, Option(current.firstThrow.get + next.firstThrow.get + last.firstThrow.getOrElse(holder.firstExtraThrow.get)), SCORE_CALCULATED)
          case (STRIKE, OPEN | SPARE, _) => Frame(current.number, current.firstThrow, current.secondThrow, Option(current.firstThrow.get + next.firstThrow.get + next.secondThrow.get), SCORE_CALCULATED)
          case (SPARE, OPEN | IN_PROGRESS | SPARE | STRIKE, _) => Frame(current.number, current.firstThrow, current.secondThrow, Option(current.firstThrow.get + current.secondThrow.get + next.firstThrow.get), SCORE_CALCULATED)
          case (OPEN, _, _) => Frame(current.number, current.firstThrow, current.secondThrow, Option(current.firstThrow.get + current.secondThrow.get), SCORE_CALCULATED);
          case (_, _, _) => Frame(current.number, current.firstThrow, current.secondThrow, current.score, current.frameState);
        }
      })
      .toList
  }
}
