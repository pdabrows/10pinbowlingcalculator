package domain

import domain.TenPinBowlingResultHolder.initFrames

import scala.util.Try

case class TenPinBowlingResultHolder(val frameList: List[Frame]= initFrames(),firstExtraThrow: Option[Int] = Option.empty, secondExtraThrow: Option[Int] = Option.empty  ) {

  def getFrame(frameNumber: Int): Option[Frame] = frameList.find(it => it.number == frameNumber)

  def getFrameList: List[Frame] = frameList

  def getFirstExtraThrow: Option[Int] = firstExtraThrow

  def getSecondExtraThrow: Option[Int] = secondExtraThrow


  def recordThrow(numberOfPins: Int): Try[TenPinBowlingResultHolder] = {
    val lastFrame = frameList.find(it => it.number == 10).get
    Try {
      lastFrame.frameState match {
        case FrameState.STRIKE if firstExtraThrow.nonEmpty => saveSecondExtraThrow(numberOfPins)
        case FrameState.SPARE | FrameState.STRIKE => saveFirstExtraThrow(numberOfPins)
        case FrameState.OPEN => throw new IllegalStateException("All throws has been used")
        case _ => saveFrameThrow(numberOfPins)
      }
    }
  }

  private def saveFrameThrow(numberOfPins: Int) = {
    saveThrowInFrames(numberOfPins)
  }

  private def saveFirstExtraThrow(numberOfPins: Int) = {
    this.copy(firstExtraThrow = Option(numberOfPins))
  }

  private def saveSecondExtraThrow(numberOfPins: Int) = {
    this.copy(secondExtraThrow = Option(numberOfPins))
  }

  private def saveThrowInFrames(numberOfPins: Int): TenPinBowlingResultHolder = {
    val recalculatedFrames = this.frameList
      .find(it => it.frameState == FrameState.IN_PROGRESS)
      .orElse(frameList.find(frame => frame.frameState == FrameState.NO_STARTED))
      .map(frameToUpdate => frameList.map {
        case Frame(frameToUpdate.number, None, None, _, _) if numberOfPins == 10 => (Frame(frameToUpdate.number, Option(numberOfPins), Option.empty, Option.empty, FrameState.STRIKE))
        case Frame(frameToUpdate.number, None, None, _, _) => (Frame(frameToUpdate.number, Option(numberOfPins), Option.empty, Option.empty, FrameState.IN_PROGRESS))
        case Frame(frameToUpdate.number, Some(value), None, _, _) if value + numberOfPins == 10 => (Frame(frameToUpdate.number, Some(value), Option(numberOfPins), Option.empty, FrameState.SPARE))
        case Frame(frameToUpdate.number, Some(value), None, _, _) => (Frame(frameToUpdate.number, Some(value), Option(numberOfPins), Option.empty, FrameState.OPEN))
        case it => it
      })
      .get
    this.copy(frameList = recalculatedFrames)
  }
}

object TenPinBowlingResultHolder {
  def initFrames(): List[Frame] = {
  Range.inclusive(1, 10)
  .map(it => Frame(number = it))
  .toList
}
}