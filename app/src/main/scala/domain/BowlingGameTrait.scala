package domain

import scala.util.Try

trait BowlingGameTrait {
  def recordThrow(pins: Int): Try[Int]
  def score(): Int
}
