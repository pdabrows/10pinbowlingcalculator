package domain

import scala.util.{Failure, Success, Try}

class TenPinBowlingGame extends BowlingGameTrait {
  private var tenPinBowlingResultHolder: TenPinBowlingResultHolder = TenPinBowlingResultHolder()
  private val tenPinBowlingFramesScoreCalculator: TenPinBowlingFramesScoreCalculator = new TenPinBowlingFramesScoreCalculator()

  override def recordThrow(pins: Int): Try[Int] = {
    tenPinBowlingResultHolder.recordThrow(pins) match {
      case Failure(exception) => Failure(exception)
      case Success(value) => tenPinBowlingResultHolder = tenPinBowlingFramesScoreCalculator.recalculateFramesScore(value)
        Success(pins)
    }
  }

  override def score(): Int = {
    tenPinBowlingResultHolder.frameList
      .filter(it => it.frameState == FrameState.SCORE_CALCULATED)
      .flatMap(it => it.score)
      .sum
  }
}
