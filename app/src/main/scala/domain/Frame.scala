package domain

import domain.FrameState.{FrameState, NO_STARTED}

case class Frame(number: Int,
                 firstThrow: Option[Int] = Option.empty,
                 secondThrow: Option[Int] = Option.empty,
                 score: Option[Int] = Option.empty,
                 frameState: FrameState = NO_STARTED
                ) {


}

object FrameState extends Enumeration {
  type FrameState = Value
  val NO_STARTED, IN_PROGRESS, STRIKE, SPARE, OPEN, SCORE_CALCULATED  = Value
}
