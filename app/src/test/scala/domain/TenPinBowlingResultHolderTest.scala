package domain

import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funsuite.AnyFunSuite
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TenPinBowlingResultHolderTest extends AnyFunSuite with BeforeAndAfterEach with BowlingResultHolderTrait {
  var resultHolder: TenPinBowlingResultHolder = _

  override def beforeEach(): Unit = {
    resultHolder = new TenPinBowlingResultHolder
  }

  test("Should save number of pins of single throw and return success") {
    //when
    val result = resultHolder.recordThrow(5)
    //then
    assert(result.isSuccess)
    result.map(it => assert(it.getFrame(1).get == Frame(number = 1,
      firstThrow = Option(5),
      secondThrow = Option.empty,
      score = Option.empty,
      frameState = FrameState.IN_PROGRESS
    )))
  }

  test("Should save number of pins both throw to the current frame and set status OPEN when is not a spare nor strike") {

    //when
    val currentResult = resultHolder
      .recordThrow(5)
      .flatMap(it => it.recordThrow(4))

    //then
    assert(currentResult.isSuccess)
    currentResult.map(it =>
      assert(it.getFrame(1).get == Frame(number = 1,
        firstThrow = Option(5),
        secondThrow = Option(4),
        score = Option.empty,
        frameState = FrameState.OPEN
      )))
  }

  test("Should save number of pins in both throws to the current frame and set status SPARE when sum of both is 10") {

    //when
    val currentResult = resultHolder
      .recordThrow(5)
      .flatMap(it => it.recordThrow(4))

    //then
    assert(currentResult.isSuccess)
    currentResult.map(it =>
      assert(it.getFrame(1).get == Frame(number = 1,
        firstThrow = Option(6),
        secondThrow = Option(4),
        score = Option.empty,
        frameState = FrameState.SPARE
      )))
  }

  test("Should save throw to next frame and set status IN_PROGRESS when there was a strike") {
    //given

    //when
    val currentResult = resultHolder
      .recordThrow(10)
      .flatMap(it => it.recordThrow(4))

    //then
    currentResult.map(it =>
      assert(it.getFrame(1).get == Frame(number = 1,
        firstThrow = Option(10),
        secondThrow = Option.empty,
        score = Option.empty,
        frameState = FrameState.STRIKE
      )))

    currentResult.map(it =>
      assert(it.getFrame(1).get == Frame(number = 2,
        firstThrow = Option(5),
        secondThrow = Option.empty,
        score = Option.empty,
        frameState = FrameState.IN_PROGRESS
      )))
  }

  test("Should save 10 frames when no STRIKE or SPARE in last frame") {
    //given
    // F1 (10, x) F2 (7, 3) F3 (7, 2) F4 (9, 1) F5 (10, x) F6 (10, x) F7 (10, x) F8 (2, 3) F9 (6, 4) F10 (7, 2)
    val list = List(10, 7, 3, 7, 2, 9, 1, 10, 10, 10, 2, 3, 6, 4, 7, 2)
    //when
    val triedHolder = generateResultHolderForGivenThrows(list)

    //then
    assert(triedHolder.isSuccess)
    triedHolder.map(it =>
      assert(it.getFrame(1).get == Frame(number = 1,
        firstThrow = Option(10),
        secondThrow = Option.empty,
        score = Option.empty,
        frameState = FrameState.STRIKE
      )))

    triedHolder.map(it =>
      assert(it.getFrame(1).get == Frame(number = 10,
        firstThrow = Option(7),
        secondThrow = Option(2),
        score = Option.empty,
        frameState = FrameState.OPEN
      )))

  }

  test("Should save last throw as extraThrow when in last frame was STRIKE or SPARE") {
    //given

    // F1 (10, x) F2 (7, 3) F3 (7, 2) F4 (9, 1) F5 (10, x) F6 (10, x) F7 (10, x) F8 (2, 3) F9 (6, 4) F10 (7, 3) EX(1)
    val list = List(10, 7, 3, 7, 2, 9, 1, 10, 10, 10, 2, 3, 6, 4, 7, 3, 1)
    //when
    val triedHolder = generateResultHolderForGivenThrows(list)
    //then
    assert(triedHolder.isSuccess)
    triedHolder
      .map(it => assert(it.getFirstExtraThrow == Option(1)))
  }

  test("Should save last 2 throw as extraThrow when in last frame was STRIKE") {
    //given
    // F1 (10, x) F2 (7, 3) F3 (7, 2) F4 (9, 1) F5 (10, x) F6 (10, x) F7 (10, x) F8 (2, 3) F9 (6, 4) F10 (10, 0) EX(1,2)
    val list = List(10, 7, 3, 7, 2, 9, 1, 10, 10, 10, 2, 3, 6, 4, 10, 1, 2)
    //when
    val triedHolder = generateResultHolderForGivenThrows(list)
    //then
    assert(triedHolder.isSuccess)
    triedHolder
      .map(it => {
        assert(it.getFirstExtraThrow == Option(1))
        assert(it.getSecondExtraThrow == Option(2))
      })
  }


  test("Should throw exception when try to make additional throw but there was no SPARE or STRIKE in last throw") {
    //given

    // F1 (10, x) F2 (7, 3) F3 (7, 2) F4 (9, 1) F5 (10, x) F6 (10, x) F7 (10, x) F8 (2, 3) F9 (6, 4) F10 (2, 1) EX(1)

    val list = List(10, 7, 3, 7, 2, 9, 1, 10, 10, 10, 2, 3, 6, 4, 2, 1)
    val triedHolder = generateResultHolderForGivenThrows(list)
    //when
    val result = triedHolder
      .flatMap(it => it.recordThrow(1))

    //then
    assert(result.isFailure)
    result.map(it => assert(it.getFirstExtraThrow.isEmpty))
  }
}
