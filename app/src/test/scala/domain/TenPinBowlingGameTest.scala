package domain

import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funsuite.AnyFunSuite
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TenPinBowlingGameTest extends AnyFunSuite with BeforeAndAfterEach {


  test("Successful call of recordThrow should return number of pins") {
    //given
    val tenPinBowlingGame = new TenPinBowlingGame()

    //when
    val triedInt = tenPinBowlingGame.recordThrow(2)

    //then
    assert(triedInt.isSuccess)
    assert(triedInt.get == 2)
  }

  test("Unsuccessful call of recordThrow should return failure") {
    //given
    val tenPinBowlingGame = new TenPinBowlingGame()
    val list = List(10, 7, 3, 7, 2, 9, 1, 10, 10, 10, 2, 3, 6, 4, 7, 3, 3)

    recordThrows(tenPinBowlingGame, list)
    //when
    val triedInt = tenPinBowlingGame.recordThrow(2)

    //then
    assert(triedInt.isFailure)
  }

  test("Score should be zero when no frames has been finished") {
    //given
    val tenPinBowlingGame = new TenPinBowlingGame()
    //when
    tenPinBowlingGame.recordThrow(2)
    //then
    assert(tenPinBowlingGame.score() == 0)
  }

  test("Score should be calculated first frame is closed") {
    //given
    val tenPinBowlingGame = new TenPinBowlingGame()
    //when
    recordThrows(tenPinBowlingGame, List(2, 4))
    //then
    assert(tenPinBowlingGame.score() == 6)
  }

  test("Score should be calculated correct score for full game") {
    //given
    val tenPinBowlingGame = new TenPinBowlingGame()
    val list = List(10, 7, 3, 7, 2, 9, 1, 10, 10, 10, 2, 3, 6, 4, 7, 3, 3)
    //when
    recordThrows(tenPinBowlingGame, list)
    //then
    assert(tenPinBowlingGame.score() == 168)
  }

  test("Score should be calculated only for finished frames") {
    //given
    val tenPinBowlingGame = new TenPinBowlingGame()
    val list = List(10, 7, 3, 7, 2, 9, 1, 10, 10, 10, 2, 3, 6, 4, 7, 3)
    //when
    recordThrows(tenPinBowlingGame, list)
    //then
    assert(tenPinBowlingGame.score() == 155)
  }

  private def recordThrows(game: TenPinBowlingGame, list: List[Int]): Unit = {
    list.foreach(it =>
      game.recordThrow(it)
    )
  }


}
