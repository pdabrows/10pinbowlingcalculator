package domain

import scala.util.Try

trait BowlingResultHolderTrait {

   def generateResultHolderForGivenThrows(list: List[Int]): Try[TenPinBowlingResultHolder] = {
    var holder = Try {
      TenPinBowlingResultHolder()
    }
    list.foreach(number => {
      holder = holder
        .flatMap(it => it.recordThrow(number))
    })
    holder
  }
}
